package com.osoboo.jira.metadata;

import org.junit.Test;
import org.openqa.selenium.By;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;


@WebTest(value = { Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PROJECTS, Category.PLUGINS })
public class MetadataViewerTest extends AbstractMetadataTestCase {

    @Override
    public void doSetUpTest() {
        jira.backdoor().restoreDataFromResource("2016_02_21_base_with_metadata_view.zip");
        jira.backdoor().websudo().disable();
        jira.gotoLoginPage().loginAsSysadminAndGoToHome();
    }


    @Test
    public void testHideReporterPhoneNumber() {
        jira.backdoor().plugins().enablePluginModule(
                "com.osoboo.jira-metadata-plugin:metadata-viewer-section");
        gotoUrl("/browse/SP-1");
        waitForElement(By.id("content"));
        assertTextPresent("Sample Feature");
        assertTextPresent("Metadata View");
        assertTextPresent("Reporter Phone");
        assertTextPresent("555-598-587");

        gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin%3AJIRA-Metadata-project-tab");
        waitAndClick(By.id("add-metadata"));
        setFormElement(By.id("metadataKey"), "phone.visibleTo");
        setFormElement(By.id("metadataValue"), "Developers");
        click(By.id("save_metadata"));

        gotoUrl("/browse/SP-1");
        waitForElement(By.id("content"));
        assertTextPresent("Sample Feature");
        assertTextPresent("Metadata View");
        assertTextPresent("Reporter Phone");
        assertTextPresent("555-598-587");

        jira.logout();
        jira.gotoLoginPage().loginAndGoToHome("test", "test");
        gotoUrl("/browse/SP-1");
        waitForElement(By.id("content"));
        assertTextPresent("Sample Feature");
        assertTextNotPresent("Reporter Phone");
        assertTextNotPresent("555-598-587");
    }


    @Test
    public void testRenameMetadataView() {
        jira.backdoor().plugins().enablePluginModule(
                "com.osoboo.jira-metadata-plugin:metadata-viewer-section");
        gotoUrl("/browse/SP-1");
        waitForElement(By.id("content"));
        assertTextPresent("Sample Feature");
        assertTextPresent("Metadata View");
        assertTextPresent("Reporter Phone");
        assertTextPresent("555-598-587");
        
        gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin%3AJIRA-Metadata-project-tab");
        waitAndClick(By.id("add-metadata"));
        setFormElement(By.id("metadataKey"), "meta.view.label");
        setFormElement(By.id("metadataValue"), "My MetadataView Name");
        click(By.id("save_metadata"));
        
        gotoUrl("/browse/SP-1");
        waitForElement(By.id("content"));
        assertTextPresent("Sample Feature");
        assertTextNotPresent("Metadata View");
        assertTextPresent("My MetadataView Name");
        assertTextPresent("Reporter Phone");
        assertTextPresent("555-598-587");

    }


    @Test
    public void testHideMetadataView() {
        jira.backdoor().plugins().enablePluginModule(
                "com.osoboo.jira-metadata-plugin:metadata-viewer-section");
        gotoUrl("/browse/SP-1");
        waitForElement(By.id("content"));
        assertTextPresent("Sample Feature");
        assertTextPresent("Metadata View");
        assertTextPresent("Reporter Phone");
        assertTextPresent("555-598-587");

        jira.backdoor().plugins().disablePluginModule(
                "com.osoboo.jira-metadata-plugin:metadata-viewer-section");
        gotoUrl("/browse/SP-1");
        waitForElement(By.id("content"));
        assertTextPresent("Sample Feature");
        assertTextNotPresent("Metadata View");
        assertTextNotPresent("Reporter Phone");
        assertTextNotPresent("555-598-587");

    }


}
