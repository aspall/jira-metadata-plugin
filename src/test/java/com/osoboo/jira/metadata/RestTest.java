package com.osoboo.jira.metadata;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.pages.JiraLoginPage;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.util.UrlUtils;
import org.junit.Assert;
import org.junit.Test;

import javax.xml.bind.DatatypeConverter;

import java.io.IOException;


@WebTest(value = { Category.WEBDRIVER_TEST, Category.API, Category.REST })
public class RestTest extends AbstractMetadataTestCase {


    protected boolean shouldSkipSetup() {
        return true;
    }


    public void doSetUpTest() {
        jira.backdoor().restoreDataFromResource("2016_02_21_jql_setting.zip");
        jira.backdoor().websudo().disable();
        jira.gotoLoginPage().loginAsSysadminAndGoToHome();
    }


    @Test
    public void testVersionRest() {

        gotoUrlBodyWait("/rest/metadata/1.0/version/10001?key=versionMetadata", "myMetaValue");
        assertTextPresent("myMetaValue");

        gotoUrlBodyWait("/rest/metadata/1.0/version/10001?key=projectMetadata", "");
        assertTextNotPresent("myMetaValue");
    }


    @Test
    public void testComponentRest() {
        gotoUrlBodyWait("/rest/metadata/1.0/component/10000?key=componentMetadata", "myMetaValue");
        assertTextPresent("myMetaValue");

        gotoUrlBodyWait("/rest/metadata/1.0/component/10000?key=projectMetadata", "");
        assertTextNotPresent("myMetaValue");

    }


    @Test
    public void testProjectRest() {
        gotoUrlBodyWait("/rest/metadata/1.0/project/SP?key=projectMetadata", "myMetaValue");
        assertTextPresent("myMetaValue");

        gotoUrlBodyWait("/rest/metadata/1.0/project/SP?key=userMetadata", "");
        assertTextNotPresent("myMetaValue");
    }


    @Test
    public void testUserRest() {
        gotoUrlBodyWait("/rest/metadata/1.0/user/admin?key=userMetadata", "myMetaValue");
        assertTextPresent("myMetaValue");

        gotoUrlBodyWait("/rest/metadata/1.0/user/admin?key=projectMetadata", "");
        assertTextNotPresent("myMetaValue");
    }


    @Test
    public void testUserDeleteRest() {
        gotoUrlBodyWait("/rest/metadata/1.0/user/admin?key=userMetadata", "myMetaValue");
        assertTextPresent("myMetaValue");

        try {
            callDelete("/rest/metadata/1.0/user/admin?key=userMetadata");
        } catch (Exception e) {
            Assert.fail("exception while call delete");
        }

        gotoUrlBodyWait("/rest/metadata/1.0/user/admin?key=userMetadata", "");
        assertTextNotPresent("myMetaValue");
    }


    private static String base64Encode(String stringToEncode) {
        return DatatypeConverter.printBase64Binary(stringToEncode.getBytes());
    }


    public void callDelete(String relativeURL) throws FailingHttpStatusCodeException, IOException {
        String url = jira.getProductInstance().getBaseUrl() + relativeURL;
        WebClient webClient = new WebClient();
        String base64encodedUsernameAndPassword = base64Encode(JiraLoginPage.USER_ADMIN + ":" + JiraLoginPage.PASSWORD_ADMIN);
        webClient.addRequestHeader("Authorization", "Basic " + base64encodedUsernameAndPassword);
        WebRequest request = new WebRequest(UrlUtils.toUrlSafe(url), HttpMethod.DELETE);
        Page page = webClient.getPage(request);
    }


}
