package com.osoboo.jira.metadata;

import org.junit.Test;
import org.openqa.selenium.By;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;


@WebTest(value = { Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PROJECTS, Category.PLUGINS })
public class UserTest extends AbstractMetadataTestCase {

    protected boolean shouldSkipSetup() {
        return true;
    }


    public void doSetUpTest() {
        jira.backdoor().restoreDataFromResource("2016_02_21_base_with_project_subtask.zip");
        jira.backdoor().websudo().disable();
        jira.gotoLoginPage().loginAsSysadminAndGoToHome();
    }


    @Test
    public void testCreateEditDeleteMetadata() {
        // select user
        gotoUrl("/secure/admin/metadata/UserAction!default.jspa");
        waitSetFormElement(By.id("selectedUser"), "test");
        click(By.id("select_user"));
        waitForElement(By.id("add-metadata"));
        assertTextPresent("There are no metadata for the user");

        waitAndClick(By.id("add-metadata"));
        setFormElement(By.id("metadataKey"), "create-edit-delete");
        setFormElement(By.id("metadataValue"), "some value");
        click(By.id("save_metadata"));

        waitForElement(By.id("add-metadata"));
        assertTextPresent("create-edit-delete");
        assertTextPresent("some value");


        // edit metadata
        waitAndClick(By.id("edit-create_edit_delete"));
        setFormElement(By.id("metadataValue"), "some other value");
        click(By.id("save_metadata"));
        waitForElement(By.id("add-metadata"));
        assertTextPresent("create-edit-delete");
        assertTextNotPresent("some value");
        assertTextPresent("some other value");

        // view other user
        gotoUrl("/secure/admin/metadata/UserAction.jspa?selectedUser=admin");
        waitForElement(By.id("add-metadata"));
        assertTextNotPresent("create-edit-delete");
        assertTextNotPresent("some value");
        assertTextNotPresent("some other value");

        // go back and delete metadata
        gotoUrl("/secure/admin/metadata/UserAction.jspa?selectedUser=test");
        waitAndClick(By.id("delete-create_edit_delete"));
        jira.getTester().getDriver().switchTo().alert().accept();
        waitForElement(By.id("add-metadata"));

        gotoUrl("/secure/admin/metadata/UserAction.jspa?selectedUser=test");
        waitForElement(By.id("add-metadata"));
        assertTextNotPresent("create-edit-delete");
        assertTextNotPresent("some value");
        assertTextNotPresent("some other value");
    }

}
