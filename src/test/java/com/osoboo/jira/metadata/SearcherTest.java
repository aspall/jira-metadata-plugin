package com.osoboo.jira.metadata;

import static org.junit.Assert.assertTrue;

import java.util.UUID;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.pages.admin.customfields.ConfigureFieldDialog;
import com.atlassian.jira.pageobjects.pages.admin.customfields.TypeSelectionCustomFieldDialog;
import com.atlassian.jira.pageobjects.pages.admin.customfields.ViewCustomFields;
import com.atlassian.webdriver.utils.element.ElementIsVisible;


@WebTest(value = { Category.WEBDRIVER_TEST, Category.INDEXING, Category.PROJECTS, Category.PLUGINS })
public class SearcherTest extends AbstractMetadataTestCase {



    protected boolean shouldSkipSetup() {
        return true;
    }

    public void doSetUpTest() {
        jira.backdoor().restoreDataFromResource("2016_02_21_base_with_project.zip");
        jira.backdoor().websudo().disable();
        jira.gotoLoginPage().loginAsSysadminAndGoToHome();
    }


    @Test
    public void testTextSearcher() {
        String randomKey = new StringBuilder("PK-23-Sys-3").append(UUID.randomUUID().toString()).toString();
        // create metadata
        initMetadata(randomKey);
        initCustomField("com.osoboo.jira-metadata-plugin:JIRA-Metadata-view-TextSearcher");

        gotoUrlBodyWait("/rest/api/2/search?jql=\"Account Info\" is EMPTY");
        assertTextNotPresent("Sample Feature");

        gotoUrlBodyWait("/rest/api/2/search?jql=\"Account Info\" is not EMPTY");
        assertTextPresent("Sample Feature");

        gotoUrlBodyWait("/rest/api/2/search?jql=\"Account Info\" ~ " + randomKey);
        assertTextPresent("Sample Feature");
    }


    @Test
    public void testExactNumberSearcher() {
        String number = "24345.08";
        initMetadata(number);
        initCustomField("com.osoboo.jira-metadata-plugin:JIRA-Metadata-view-ExactNumberSearcher");

        gotoUrlBodyWait("/rest/api/2/search?jql=\"Account Info\" is EMPTY");
        assertTextNotPresent("Sample Feature");

        gotoUrlBodyWait("/rest/api/2/search?jql=\"Account Info\" is not EMPTY");
        assertTextPresent("Sample Feature");

        gotoUrlBodyWait("/rest/api/2/search?jql=\"Account Info\" = \"" + number + "\"");
        assertTextPresent("Sample Feature");

        gotoUrlBodyWait("/rest/api/2/search?jql=\"Account Info\" < \"124345\"");
        assertTextPresent("Sample Feature");

        gotoUrlBodyWait("/rest/api/2/search?jql=\"Account Info\" < \"14345\"");
        assertTextNotPresent("Sample Feature");

        gotoUrlBodyWait("/rest/api/2/search?jql=\"Account Info\" > \"4345\"");
        assertTextPresent("Sample Feature");
    }


    @Test
    public void testRangeNumberSearcher() {
        String number = "24345.08";
        initMetadata(number);
        initCustomField("com.osoboo.jira-metadata-plugin:JIRA-Metadata-view-NumberRangeSearcher");

        gotoUrlBodyWait("/rest/api/2/search?jql=\"Account Info\" is EMPTY");
        assertTextNotPresent("Sample Feature");

        gotoUrlBodyWait("/rest/api/2/search?jql=\"Account Info\" <= \"50000\" AND \"Account Info\" >= \"123\"");
        assertTextPresent("Sample Feature");
    }


    private void initCustomField(String searcherKey) {
        // 1. custom field add
        ViewCustomFields viewCustomFields = pageBinder.navigateToAndBind(ViewCustomFields.class);
        TypeSelectionCustomFieldDialog customFieldDialog = viewCustomFields.addCustomField();
        // select all (the first entry)
        click(By.cssSelector("button.item-button"));
        customFieldDialog.select("JIRA Metadata - View project metadata field");
        ConfigureFieldDialog configureFieldDialog = customFieldDialog.next();
        configureFieldDialog.name("Account Info");
        // set the screen
        configureFieldDialog.nextAndThenAssociate();
        click(By.name("associatedScreens"));
        clicknHide(By.id("license-flag-never"));
        
        click(By.id("update_submit"));

        // the text searcher
        waitAndClick(By.cssSelector("button[aria-controls=\"field-actions-customfield_10000\"]"));
        click(By.id("edit_Account Info"));
        new WebDriverWait(jira.getTester().getDriver(), 60).until(new ElementIsVisible(By.name("searcher"),
                null));
        new Select(jira.getTester().getDriver().findElement(By.name("searcher"))).selectByValue(searcherKey);
        click(By.id("update_submit"));

        // // configure 1. custom field
        waitAndClick(By.cssSelector("button[aria-controls=\"field-actions-customfield_10000\"]"));
        click(By.id("config_customfield_10000"));

        waitAndClick(By.id("customfield_10000-edit-default"));
        new WebDriverWait(jira.getTester().getDriver(), 60).until(new ElementIsVisible(By
                .id("customfield_10000"), null));
        setFormElement(By.id("customfield_10000"), "accounting.code");
        click(By.id("set_defaults_submit"));

        reindex();
    }


    private void initMetadata(String randomKey) {
        jira.getTester()
                .gotoUrl(
                        jira.getProductInstance().getBaseUrl()
                                + "/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin%3AJIRA-Metadata-project-tab");

        waitAndClick(By.id("add-metadata"));
        setFormElement(By.id("metadataKey"), "accounting.code");
        setFormElement(By.id("metadataValue"), randomKey);
        click(By.id("save_metadata"));
        waitForElement(By.id("add-metadata"));
        assertTrue(jira.getTester().getDriver().getPageSource().contains("accounting.code"));
        assertTrue(jira.getTester().getDriver().getPageSource().contains(randomKey));
    }

}
