package com.osoboo.jira.metadata;

import java.util.UUID;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.pages.admin.customfields.ConfigureFieldDialog;
import com.atlassian.jira.pageobjects.pages.admin.customfields.TypeSelectionCustomFieldDialog;
import com.atlassian.jira.pageobjects.pages.admin.customfields.ViewCustomFields;
import com.atlassian.webdriver.utils.element.ElementIsVisible;


/**
 * checks whether the lookup of an other custom field is possible
 * 
 * @author aspall
 * 
 */
@WebTest(value = { Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PROJECTS, Category.PLUGINS })
public class CustomFieldLookupTest extends AbstractMetadataTestCase {

    protected boolean shouldSkipSetup() {
        return true;
    }


    public void doSetUpTest() {
        jira.backdoor().restoreDataFromResource("2016_02_21_base_with_project.zip");
        jira.backdoor().websudo().disable();
        jira.gotoLoginPage().loginAsSysadminAndGoToHome();
    }


    @Test
    public void testLookupOtherCustomField() {
        String randomKey = new StringBuilder("PK-23-Sys-3").append(UUID.randomUUID().toString()).toString();

        // 1. custom field add
        ViewCustomFields viewCustomFields = pageBinder.navigateToAndBind(ViewCustomFields.class);
        TypeSelectionCustomFieldDialog customFieldDialog = viewCustomFields.addCustomField();
        // select all (the first entry)
        click(By.cssSelector("button.item-button"));
        customFieldDialog.select("JIRA Metadata - View project metadata field");
        ConfigureFieldDialog configureFieldDialog = customFieldDialog.next();
        configureFieldDialog.name("Account Info");
        // set the screen
        configureFieldDialog.nextAndThenAssociate();
        clicknHide(By.id("license-flag-never"));
        click(By.name("associatedScreens"));

        click(By.id("update_submit"));

        waitAndClick(By.cssSelector("button[aria-controls=\"field-actions-customfield_10000\"]"));
        click(By.id("config_customfield_10000"));
        waitAndClick(By.id("customfield_10000-edit-default"));
        new WebDriverWait(jira.getTester().getDriver(), 60).until(new ElementIsVisible(By
                .id("customfield_10000"), null));
        setFormElement(By.id("customfield_10000"), "accounting.code");
        click(By.id("set_defaults_submit"));

        // create metadata
        gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin%3AJIRA-Metadata-project-tab");
        waitAndClick(By.id("add-metadata"));
        setFormElement(By.id("metadataKey"), "accounting.code");
        setFormElement(By.id("metadataValue"), randomKey);
        click(By.id("save_metadata"));
        waitForElement(By.id("jira-metadata"));
        assertTextPresent("accounting.code");
        assertTextPresent(randomKey);

        // im Issue
        gotoUrl("/browse/SP-1");
        assertTextPresent("Sample Feature");
        assertTextPresent("Account Info");
        assertTextNotPresent("accounting.code");
        assertTextPresent(randomKey);

        reindex();

        // 2. custom field add        
        viewCustomFields = pageBinder.navigateToAndBind(ViewCustomFields.class);
        customFieldDialog = viewCustomFields.addCustomField();
        // select all (the first entry)
        click(By.cssSelector("button.item-button"));
        customFieldDialog.select("JIRA Metadata - Calc. field");
        configureFieldDialog = customFieldDialog.next();
        configureFieldDialog.name("Amount");
        // set the screen
        configureFieldDialog.nextAndThenAssociate();
        clicknHide(By.id("license-flag-never"));
        click(By.name("associatedScreens"));
        click(By.id("update_submit"));

        gotoUrl("/admin/ConfigureCustomField!default.jspa?customFieldId=10001");
        waitAndClick(By.id("customfield_10001-edit-default"));
        new WebDriverWait(jira.getTester().getDriver(), 60).until(new ElementIsVisible(By
                .id("customfield_10001"), null));
        setFormElement(By.id("customfield_10001"),
                "PRE $!customFieldLookupService.getCustomFieldValue($issue, 'customfield_10000') POST");
        click(By.id("set_defaults_submit"));


        // im Issue
        gotoUrl("/browse/SP-1");
        assertTextPresent("Sample Feature");
        assertTextPresent("Account Info");
        assertTextNotPresent("accounting.code");
        assertTextPresent(randomKey);
        assertTextPresent("PRE " + randomKey + " POST");
    }
}
