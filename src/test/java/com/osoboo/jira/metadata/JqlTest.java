package com.osoboo.jira.metadata;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.Test;


@WebTest(value = { Category.WEBDRIVER_TEST, Category.INDEXING, Category.PROJECTS, Category.PLUGINS })
public class JqlTest extends AbstractMetadataTestCase {



    protected boolean shouldSkipSetup() {
        return true;
    }

    public void doSetUpTest() {
        jira.backdoor().restoreDataFromResource("2016_02_21_jql_setting.zip");
        jira.backdoor().websudo().disable();
        jira.gotoLoginPage().loginAsSysadminAndGoToHome();
    }


    @Test
    public void testVersionJQL() {
        
        gotoUrlBodyWait("/rest/api/2/search?jql=");
        assertTextPresent("Sample Feature");
        assertTextPresent("Sample Subtask");
        assertTextPresent("My Bug");
        
        gotoUrlBodyWait("/rest/api/2/search?jql=fixVersion in versionsMetadata(\"versionMetadata\", \"myMetaValue\")");
        assertTextNotPresent("Sample Feature");
        assertTextNotPresent("Sample Subtask");
        assertTextPresent("My Bug");

        gotoUrlBodyWait("/rest/api/2/search?jql=fixVersion in versionsMetadata(\"versionMetadata\", \"otherMetaValue\")");
        assertTextNotPresent("Sample Feature");
        assertTextNotPresent("Sample Subtask");
        assertTextNotPresent("My Bug");
    }

    @Test
    public void testComponentJQL() {
        
        gotoUrlBodyWait("/rest/api/2/search?jql=");
        assertTextPresent("Sample Feature");
        assertTextPresent("Sample Subtask");
        assertTextPresent("My Bug");
        
        gotoUrlBodyWait("/rest/api/2/search?jql=component in componentsMetadata(\"componentMetadata\", \"myMetaValue\")");
        assertTextNotPresent("Sample Feature");
        assertTextNotPresent("Sample Subtask");
        assertTextPresent("My Bug");

        gotoUrlBodyWait("/rest/api/2/search?jql=component in componentsMetadata(\"componentMetadata\", \"otherMetaValue\")");
        assertTextNotPresent("Sample Feature");
        assertTextNotPresent("Sample Subtask");
        assertTextNotPresent("My Bug");
    }

    @Test
    public void testProjectJQL() {
        
        gotoUrlBodyWait("/rest/api/2/search?jql=");
        assertTextPresent("Sample Feature");
        assertTextPresent("Sample Subtask");
        assertTextPresent("My Bug");
        
        gotoUrlBodyWait("/rest/api/2/search?jql=project in projectsMetadata(\"projectMetadata\", \"myMetaValue\")");
        assertTextPresent("Sample Feature");
        assertTextPresent("Sample Subtask");
        assertTextPresent("My Bug");

        gotoUrlBodyWait("/rest/api/2/search?jql=project in projectsMetadata(\"projectMetadata\", \"otherMetaValue\")");
        assertTextNotPresent("Sample Feature");
        assertTextNotPresent("Sample Subtask");
        assertTextNotPresent("My Bug");
    }

    @Test
    public void testUserJQL() {
        
        gotoUrlBodyWait("/rest/api/2/search?jql=");
        assertTextPresent("Sample Feature");
        assertTextPresent("Sample Subtask");
        assertTextPresent("My Bug");
        
        gotoUrlBodyWait("/rest/api/2/search?jql=reporter in usersMetadata(\"userMetadata\", \"myMetaValue\")");
        assertTextPresent("Sample Feature");
        assertTextPresent("Sample Subtask");
        assertTextPresent("My Bug");

        gotoUrlBodyWait("/rest/api/2/search?jql=reporter in usersMetadata(\"userMetadata\", \"otherMetaValue\")");
        assertTextNotPresent("Sample Feature");
        assertTextNotPresent("Sample Subtask");
        assertTextNotPresent("My Bug");
        
        gotoUrlBodyWait("/rest/api/2/search?jql=reporter in usersMetadata(\"userHiddenMetadata\", \"myMetaValue\")");
        assertTextNotPresent("Sample Feature");
        assertTextNotPresent("Sample Subtask");
        assertTextNotPresent("My Bug");
    }

}
