package com.osoboo.jira.metadata;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.pages.project.IndexProjectPage;
import com.atlassian.jira.pageobjects.project.summary.EditProjectPageTab;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.utils.element.ElementIsVisible;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;


@WebTest(value = { Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PROJECTS, Category.PLUGINS })
public class ProjectTest extends AbstractMetadataTestCase {


    protected boolean shouldSkipSetup() {
        return true;
    }


    public void doSetUpTest() {
        jira.backdoor().restoreDataFromResource("2016_02_21_base_with_project.zip");
        jira.backdoor().websudo().disable();
        jira.gotoLoginPage().loginAsSysadminAndGoToHome();
    }


    /**
     * Metadata lost after renaming project key.
     */
    @Test
    public void testJM47() {
        // create metadata
        gotoUrl("/browse/SP/?selectedTab=com.osoboo.jira-metadata-plugin%3AJIRA-Metadata-project-tab");
        waitAndClick(By.id("add-metadata"));
        setFormElement(By.id("metadataKey"), "some key");
        setFormElement(By.id("metadataValue"), "some value");
        click(By.id("save_metadata"));

        EditProjectPageTab navigateToAndBind = pageBinder.navigateToAndBind(EditProjectPageTab.class, "10000");

        Poller.waitUntilTrue(navigateToAndBind.isProjectKeyVisible());
        navigateToAndBind.setProjectKey("SPCH").submit();

        IndexProjectPage indexProjectPage = jira.getPageBinder().bind(IndexProjectPage.class);
        indexProjectPage.acknowledge();

        gotoUrl("/browse/SPCH/?selectedTab=com.osoboo.jira-metadata-plugin%3AJIRA-Metadata-project-tab");
        new WebDriverWait(jira.getTester().getDriver(), 60).until(new ElementIsVisible(By.id("add-metadata"), null));
        assertTrue(jira.getTester().getDriver().getPageSource().contains("some value"));
    }


}
