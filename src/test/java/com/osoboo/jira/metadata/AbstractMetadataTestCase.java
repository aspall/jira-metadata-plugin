package com.osoboo.jira.metadata;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.atlassian.webdriver.utils.element.ElementNotVisible;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.NoSuchElementException;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.EnvironmentBasedProductInstance;
import com.atlassian.jira.webtest.webdriver.selenium.PageContainsCondition;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.utils.element.ElementIsVisible;


public abstract class AbstractMetadataTestCase {

    protected JiraTestedProduct jira;
    protected com.atlassian.pageobjects.PageBinder pageBinder;


    @Before
    public void setUpTest() {
        jira = TestedProductFactory.create(JiraTestedProduct.class, new EnvironmentBasedProductInstance(), null);
        pageBinder = jira.getPageBinder();
        doSetUpTest();

        jira.backdoor().plugins().disablePlugin("com.atlassian.support.stp");
        jira.backdoor().plugins().disablePlugin("com.atlassian.feedback.jira-feedback-plugin");
        jira.backdoor().plugins().disablePlugin("com.atlassian.analytics.analytics-client");
        // jira.backdoor().plugins().disablePluginModule("stp-license-status-resources");
    }


    public abstract void doSetUpTest();


    protected void assertTextNotPresent(String toFind) {
        assertFalse(pageSourceContains(toFind));
    }


    protected void assertTextPresent(String toFind) {
        assertTrue(pageSourceContains(toFind));
    }


    protected void clicknHide(By identifier) {
        try {
            jira.getTester().getDriver().findElement(identifier).click();
        } catch (NoSuchElementException e) {
        } catch (StaleElementReferenceException e) {
        }
        new WebDriverWait(jira.getTester().getDriver(), 5).until(new ElementNotVisible(identifier, null));
    }


    protected void click(By identifier) {
        jira.getTester().getDriver().findElement(identifier).click();
    }


    protected void waitForElement(By identifier) {
        new WebDriverWait(jira.getTester().getDriver(), 60).until(new ElementIsVisible(identifier, null));
    }


    protected void waitAndClick(By identifier) {
        waitForElement(identifier);
        click(identifier);
    }


    protected void reindex() {
        jira.getTester().gotoUrl(jira.getProductInstance().getBaseUrl() + "/secure/admin/IndexAdmin.jspa");
        waitAndClick(By.id("indexing-submit"));
        waitAndClick(By.id("acknowledge_submit"));
    }


    protected void gotoUrl(String url) {
        jira.getTester().gotoUrl(jira.getProductInstance().getBaseUrl() + url);
        waitForElement(By.id("content"));
    }


    protected void gotoUrlBodyWait(String url) {
        gotoUrlBodyWait(url, "body");
    }
    
    protected void gotoUrlBodyWait(String url, String containsCondition) {
        jira.getTester().gotoUrl(jira.getProductInstance().getBaseUrl() + url);
        new WebDriverWait(jira.getTester().getDriver(), 60).until(new PageContainsCondition(containsCondition));
    }


    private boolean pageSourceContains(String toFind) {
        return jira.getTester().getDriver().getPageSource().contains(toFind);
    }


    protected void setFormElement(By identifier, String text) {
        jira.getTester().getDriver().findElement(identifier).clear();
        jira.getTester().getDriver().findElement(identifier).sendKeys(text);
    }


    protected void waitSetFormElement(By identifier, String text) {
        waitForElement(identifier);
        setFormElement(identifier, text);

    }

}