package com.osoboo.jira.metadata;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.pages.admin.customfields.ConfigureFieldDialog;
import com.atlassian.jira.pageobjects.pages.admin.customfields.TypeSelectionCustomFieldDialog;
import com.atlassian.jira.pageobjects.pages.admin.customfields.ViewCustomFields;
import com.atlassian.webdriver.utils.element.ElementIsVisible;

@WebTest(value = { Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.PROJECTS, Category.PLUGINS })
public class CustomFieldTest extends AbstractMetadataTestCase {

    protected boolean shouldSkipSetup() {
        return true;
    }

	public void doSetUpTest() {
        jira.backdoor().restoreDataFromResource("2016_02_21_base_with_project_subtask.zip");
        jira.backdoor().websudo().disable();
        jira.gotoLoginPage().loginAsSysadminAndGoToHome();
	}

	@Test
	public void testCreateSecondTaskSubtask_JMETA_30() {
		// pre-check
        gotoUrl("/browse/SP-1");
		assertTextPresent("Sample Feature");
		assertTextNotPresent("Amount");

        // 1. custom field add
        ViewCustomFields viewCustomFields = pageBinder.navigateToAndBind(ViewCustomFields.class);
        TypeSelectionCustomFieldDialog customFieldDialog = viewCustomFields.addCustomField();
        // select all (the first entry)
        click(By.cssSelector("button.item-button"));
        customFieldDialog.select("JIRA Metadata - Calc. field");
        ConfigureFieldDialog configureFieldDialog = customFieldDialog.next();
        configureFieldDialog.name("Amount");
        // set the screen
        configureFieldDialog.nextAndThenAssociate();
        click(By.name("associatedScreens"));
        click(By.id("update_submit"));
        
        waitAndClick(By.cssSelector("button[aria-controls=\"field-actions-customfield_10000\"]"));
        click(By.id("config_customfield_10000"));
        waitAndClick(By.id("customfield_10000-edit-default"));
        new WebDriverWait(jira.getTester().getDriver(), 60).until(new ElementIsVisible(By
                .id("customfield_10000"), null));
        StringBuilder sb = new StringBuilder();
        sb.append("\n## The target of this JIRA-Plugin is to make it possible to create and manage meta-data within your JIRA project.");
        sb.append("\n## Currently you can't specify any meta-data, but it can be very useful to append e.g. the SVN path, ");
        sb.append("\n## accounting information or the location of the project documentation through your JIRA project.");
        sb.append("\n");
        sb.append("\n## The target of this JIRA-Plugin is to make it possible to create and manage meta-data within your JIRA project. ");
        sb.append("\n## Currently you can't specify any meta-data, but it can be very useful to append e.g. the SVN path, ");
        sb.append("\n## accounting information or the location of the project documentation through your JIRA project.");
        sb.append("\n");
        sb.append("\n## The target of this JIRA-Plugin is to make it possible to create and manage meta-data within your JIRA project. ");
        sb.append("\n## Currently you can't specify any meta-data, but it can be very useful to append e.g. the SVN path, ");
        sb.append("\n## accounting information or the location of the project documentation through your JIRA project.");
        sb.append("\n");
        sb.append("\nBla");
        setFormElement(
                By.id("customfield_10000"),
                sb.toString());
        click(By.id("set_defaults_submit"));

		// Issue
        gotoUrl("/browse/SP-1");
		assertTextPresent("Sample Feature");
		assertTextPresent("Amount");
		assertTextNotPresent("The target of this JIRA-Plugin");
		assertTextNotPresent("Second Subtask");
		assertTextPresent("Bla");
		
		
	}

}
