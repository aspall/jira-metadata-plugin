package com.osoboo.jira.metadata.jql;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.osoboo.jira.metadata.MetadataService;
import com.osoboo.jira.metadata.internal.KeyGeneratorComponent;


public class ComponentJqlFunction extends AbstractMetadataJqlFunction {

    private String classIdentifierAsString;


    public ComponentJqlFunction(MetadataService metadataService) {
        super(metadataService);
        this.classIdentifierAsString = new KeyGeneratorComponent().getClassIdentifierAsString();
    }


    @Override
    public JiraDataType getDataType() {
        return JiraDataTypes.COMPONENT;
    }


    @Override
    public String getFunctionName() {
        return "componentsMetadata";
    }


    @Override
    protected String getClassIdentifierAsString() {
        return classIdentifierAsString;
    }
}
