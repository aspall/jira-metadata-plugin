package com.osoboo.jira.metadata.jql;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.osoboo.jira.metadata.MetadataService;
import com.osoboo.jira.metadata.internal.KeyGeneratorProject;


/**
 * http://localhost:2990/jira/browse/ZP-1?jql=project%20in%20projectsMetadata%28%22meta.view%22%2C%
 * 20%22hallo%22%29
 * 
 * @author aspall
 *
 */
public class ProjectJqlFunction extends AbstractMetadataJqlFunction {

    private String classIdentifierAsString;


    public ProjectJqlFunction(MetadataService metadataService) {
        super(metadataService);
        this.classIdentifierAsString = new KeyGeneratorProject().getClassIdentifierAsString();
    }


    @Override
    public JiraDataType getDataType() {
        return JiraDataTypes.PROJECT;
    }


    @Override
    public String getFunctionName() {
        return "projectsMetadata";
    }


    @Override
    protected String getClassIdentifierAsString() {
        return classIdentifierAsString;
    }


}
