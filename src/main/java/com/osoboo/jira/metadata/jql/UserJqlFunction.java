package com.osoboo.jira.metadata.jql;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.osoboo.jira.metadata.MetadataService;
import com.osoboo.jira.metadata.internal.KeyGeneratorUser;


/**
 * test URL:
 * http://localhost:2990/jira/browse/ZP-1?jql=assignee%20in%20usersMetadata%28%22meta.view%
 * 22%2C%20%22hallo%22%29
 * 
 * @author aspall
 *
 */
public class UserJqlFunction extends AbstractMetadataJqlFunction {

    private String classIdentifierAsString;


    public UserJqlFunction(MetadataService metadataService) {
        super(metadataService);
        this.classIdentifierAsString = new KeyGeneratorUser().getClassIdentifierAsString();
    }


    @Override
    public JiraDataType getDataType() {
        return JiraDataTypes.USER;
    }


    @Override
    public String getFunctionName() {
        return "usersMetadata";
    }


    @Override
    protected String getClassIdentifierAsString() {
        return classIdentifierAsString;
    }
}
