package com.osoboo.jira.metadata.rest;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.bc.EntityNotFoundException;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.osoboo.jira.metadata.JiraMetadata;
import com.osoboo.jira.metadata.MetadataService;
import com.osoboo.jira.metadata.ui.internal.MetadataHelper;
import org.apache.log4j.Logger;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.Collection;


@Path("")
@ExperimentalApi
public class MetadataRestService {

    private static final Logger logger = Logger.getLogger(MetadataRestService.class);

    MetadataService metadataService;

    UserManager userManager;

    private GroupManager groupManager;

    private ProjectManager projectManager;

    private VersionManager versionManager;

    private PermissionManager permissionManager;

    private ProjectComponentManager projectComponentManager;

    private JiraAuthenticationContext jiraAuthenticationContext;


    public MetadataRestService(MetadataService metadataService, UserManager userManager, GroupManager groupManager,
            ProjectManager projectManager, VersionManager versionManager, ProjectComponentManager projectComponentManager,
            PermissionManager permissionManager, JiraAuthenticationContext jiraAuthenticationContext) {
        this.metadataService = metadataService;
        this.userManager = userManager;
        this.groupManager = groupManager;
        this.projectManager = projectManager;
        this.versionManager = versionManager;
        this.projectComponentManager = projectComponentManager;
        this.permissionManager = permissionManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }


    /**
     * 
     * @param username
     * @param key
     * @return
     */
    @DELETE
    @Path("/user/{username}")
    @Produces({ MediaType.TEXT_PLAIN })
    public Response deleteUserValue(@PathParam(value = "username") String username, @QueryParam(value = "key") String key) {
        try {
            ApplicationUser user = userManager.getUserByName(username);
            if ((MetadataHelper.userHasAdminPermission(permissionManager, jiraAuthenticationContext.getUser()) || user.equals(
                    jiraAuthenticationContext.getUser()))) {
                JiraMetadata metadata = metadataService.getMetadata(user, key);
                if (metadata != null) {
                    metadataService.delete(user, metadata.getID());
                    return Response.ok().build();
                } else {
                    logger.info("ignoring call to delete user value for " + username);
                    return Response.serverError().build();
                }
            }
        } catch (RuntimeException e) {
            logger.info("Exception while rest call delete user value", e);
        }
        return Response.serverError().build();
    }


    @POST
    @Path("/user/{username}")
    @Produces({ MediaType.TEXT_PLAIN })
    public Response updateUserValue(@PathParam(value = "username") String username, @QueryParam(value = "key") String key,
            @QueryParam(value = "value") String value) {
        try {
            ApplicationUser user = userManager.getUserByName(username);
            if ((MetadataHelper.userHasAdminPermission(permissionManager, jiraAuthenticationContext.getUser()) || user.equals(
                    jiraAuthenticationContext.getUser()))) {
                JiraMetadata metadata = metadataService.getMetadata(user, key);
                if (metadata != null) {
                    metadataService.save(user, key, value);
                    return Response.ok().build();
                } else {
                    logger.info("ignoring call to update user value for " + username);
                    return Response.serverError().build();
                }
            }
        } catch (RuntimeException e) {
            logger.info("Exception while rest call update user value", e);
        }
        return Response.serverError().build();
    }


    @GET
    @Path("/user/{username}")
    @Produces({ MediaType.TEXT_PLAIN })
    public Response getUserValue(@PathParam(value = "username") String username, @QueryParam(value = "key") String key) {
        ApplicationUser user = userManager.getUserByName(username);
        return Response.ok(metadataService.getMetadataValue(user, key)).build();
    }


    @GET
    @Path("/group/{groupname}")
    @Produces({ MediaType.TEXT_PLAIN })
    public Response getGroupValue(@PathParam(value = "groupname") String groupname, @QueryParam(value = "key") String key) {
        Group group = groupManager.getGroup(groupname);
        return Response.ok(metadataService.getMetadataValue(group, key)).build();
    }


    @GET
    @Path("/project/{projectkey}")
    @Produces({ MediaType.TEXT_PLAIN })
    public Response getProjectValue(@PathParam(value = "projectkey") String projectkey, @QueryParam(value = "key") String key) {
        Project project = projectManager.getProjectObjByKey(projectkey);
        return Response.ok(metadataService.getMetadataValue(project, key)).build();
    }


    @GET
    @Path("/component/{componentId}")
    @Produces({ MediaType.TEXT_PLAIN })
    @Deprecated
    public Response getComponentValue(@PathParam(value = "componentId") long componentId, @QueryParam(value = "key") String key) {
        try {
            ProjectComponent component = projectComponentManager.find(componentId);
            return Response.ok(metadataService.getMetadataValue(component, key)).build();
        } catch (EntityNotFoundException e) {
            return Response.serverError().build();
        }
    }


    @GET
    @Path("/version/{versionId}")
    @Produces({ MediaType.TEXT_PLAIN })
    @Deprecated
    public Response getVersionValue(@PathParam(value = "versionId") long versionId, @QueryParam(value = "key") String key) {
        Version version = versionManager.getVersion(versionId);
        return Response.ok(metadataService.getMetadataValue(version, key)).build();
    }


    /**
     * sample call: http://localhost:2990/jira/rest/metadata/1.0/project/SP/version/1.0
     */
    @GET
    @Path("/project/{projectkey}/version/{version}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getProjectVersionValues(@PathParam(value = "projectkey") String projectkey,
            @PathParam(value = "version") String version) {
        try {
            Project project = projectManager.getProjectByCurrentKey(projectkey);
            Version versionObject = versionManager.getVersion(project.getId(), version);
            return Response.ok(transform(metadataService.getAllMetadata(versionObject))).build();
        } catch (Exception e) {
            return Response.serverError().build();
        }
    }

    private JiraMetadataDTO[] transform(Collection<JiraMetadata> metadatas) {
        ArrayList<JiraMetadataDTO> resultList = new ArrayList<JiraMetadataDTO>(metadatas.size());
        for (JiraMetadata metadata : metadatas) {
            resultList.add(new JiraMetadataDTO(metadata.getUserKey(), metadata.getUserGrouping(), metadata.getUserValue(), metadata
                    .isHidden()));
        }
        return resultList.toArray(new JiraMetadataDTO[0]);
    }

}
