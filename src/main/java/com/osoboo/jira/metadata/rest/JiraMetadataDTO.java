package com.osoboo.jira.metadata.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "JiraMetadata")
@XmlAccessorType(XmlAccessType.FIELD)
public class JiraMetadataDTO {

    @XmlElement(name = "key") private String key;

    @XmlElement(name = "group") private String group;

    @XmlElement(name = "value") private String value;

    @XmlElement(name = "hidden") private boolean hidden;


    public JiraMetadataDTO() {
        super();
    }


    public JiraMetadataDTO(String key, String group, String value, boolean hidden) {
        super();
        this.key = key;
        this.group = group;
        this.value = value;
        this.hidden = hidden;
    }


    public String getKey() {
        return key;
    }


    public void setKey(String key) {
        this.key = key;
    }


    public String getGroup() {
        return group;
    }


    public void setGroup(String group) {
        this.group = group;
    }


    public String getValue() {
        return value;
    }


    public void setValue(String value) {
        this.value = value;
    }


    public boolean isHidden() {
        return hidden;
    }


    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }


}
