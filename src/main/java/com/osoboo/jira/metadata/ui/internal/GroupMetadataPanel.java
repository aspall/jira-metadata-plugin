/*
 * Copyright (c) 2013, Andreas Spall
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Andreas Spall nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.osoboo.jira.metadata.ui.internal;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.osoboo.jira.metadata.MetadataService;
import com.osoboo.jira.metadata.MetadataUtils;

public class GroupMetadataPanel extends JiraWebActionSupport {

	private UserManager userManager;

	private String selectedGroup;

	private Group selectedGroupObject;

	private MetadataService metadataService;
	
	private MetadataUtils metadataUtils;

	public GroupMetadataPanel(UserManager userManager,
			MetadataService metadataService) {
		this.userManager = userManager;
		this.metadataService = metadataService;
		this.metadataUtils = new MetadataUtils();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5917806137708750611L;

	@Override
	public String doDefault() throws Exception {
		transformSelectedGroup();
		return super.doDefault();
	}

	@Override
	protected String doExecute() throws Exception {
		transformSelectedGroup();
		return super.doExecute();
	}

	private void transformSelectedGroup() {
		if (selectedGroupObject == null
				|| !selectedGroupObject.getName().equalsIgnoreCase(
						selectedGroup)) {
			selectedGroupObject = userManager.getGroupObject(selectedGroup);
		}
	}

	public Group getSelectedGroupObject() {
		return selectedGroupObject;
	}

	public void setSelectedGroupObject(Group selectedGroupObject) {
	}

	public String getSelectedGroup() {
		return selectedGroup;
	}

	public void setSelectedGroup(String selectedGroup) {
		this.selectedGroup = selectedGroup;
	}

	public MetadataService getMetadataService() {
		return metadataService;
	}

	public void setMetadataService(MetadataService metadataService) {

	}

    public MetadataUtils getMetadataUtils() {
        return metadataUtils;
    }

    public void setMetadataUtils(MetadataUtils metadataUtils) {
        this.metadataUtils = metadataUtils;
    }
	
	

}
