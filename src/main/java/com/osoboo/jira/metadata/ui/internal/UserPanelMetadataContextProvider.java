package com.osoboo.jira.metadata.ui.internal;

import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.opensymphony.util.TextUtils;
import com.osoboo.jira.metadata.MetadataService;

/**
 * part of the user profile page
 * 
 * @author aspall
 *
 */
public class UserPanelMetadataContextProvider implements ContextProvider {
    
    private MetadataService metadataService;


    public UserPanelMetadataContextProvider(MetadataService metadataService) {
        this.metadataService = metadataService;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
    }


    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        context.put("textUtils", new TextUtils());
        context.put("metadatas", metadataService.getMetadata(context.get("profileUser")));

        return context;
    }
    
}
