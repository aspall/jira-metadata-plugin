/*
 * Copyright (c) 2016, Andreas Spall
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Andreas Spall nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.osoboo.jira.metadata.ui.internal;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.osoboo.jira.metadata.MetadataService;
import com.osoboo.jira.metadata.MetadataUtils;


public class ProjectVersionMetadataPanel extends JiraWebActionSupport {

    private VersionManager versionManager;
    private ProjectManager projectManager;

    private String selectedProjectVersionId;

    private String selectedProjectKey;
    private String selectedProjectVersionName;

    private Version selectedProjectVersionObject;

    private MetadataService metadataService;

    private MetadataUtils metadataUtils;


    public ProjectVersionMetadataPanel(VersionManager versionManager, ProjectManager projectManager, MetadataService metadataService) {
        this.versionManager = versionManager;
        this.projectManager = projectManager;
        this.metadataService = metadataService;
        this.metadataUtils = new MetadataUtils();
    }

    /**
     * 
     */
    private static final long serialVersionUID = 5917806137708750611L;


    @Override
    public String doDefault() throws Exception {
        transformSelectedProjectVersion();
        return super.doDefault();
    }


    @Override
    protected String doExecute() throws Exception {
        transformSelectedProjectVersion();
        return super.doExecute();
    }


    private void transformSelectedProjectVersion() {
        if (selectedProjectVersionObject == null) {
            if (selectedProjectVersionId != null) {
                selectedProjectVersionObject = versionManager.getVersion(Long.parseLong(selectedProjectVersionId));
            }
            if (selectedProjectKey != null && selectedProjectVersionName != null) {
                Project projectByCurrentKey = projectManager.getProjectByCurrentKey(selectedProjectKey);
                if (projectByCurrentKey != null) {
                    selectedProjectVersionObject = versionManager.getVersion(projectByCurrentKey.getId(), selectedProjectVersionName);
                    if (selectedProjectVersionObject != null) {
                        selectedProjectVersionId = Long.toString(selectedProjectVersionObject.getId());
                    } else {
                        selectedProjectVersionId = null;
                    }
                }
            }
        } else {
            if (selectedProjectVersionId != null && selectedProjectVersionObject.getId() != Long.parseLong(selectedProjectVersionId)) {
                selectedProjectVersionObject = versionManager.getVersion(Long.parseLong(selectedProjectVersionId));
            }
        }

    }


    public String getSelectedProjectVersionId() {
        return selectedProjectVersionId;
    }


    public void setSelectedProjectVersionId(String selectedProjectVersionId) {
        this.selectedProjectVersionId = selectedProjectVersionId;
    }


    public Version getSelectedProjectVersionObject() {
        return selectedProjectVersionObject;
    }


    public void setSelectedProjectVersionObject(Version selectedProjectVersionObject) {
        this.selectedProjectVersionObject = selectedProjectVersionObject;
    }


    public String getSelectedProjectKey() {
        return selectedProjectKey;
    }


    public void setSelectedProjectKey(String selectedProjectKey) {
        this.selectedProjectKey = selectedProjectKey;
    }


    public String getSelectedProjectVersionName() {
        return selectedProjectVersionName;
    }


    public void setSelectedProjectVersionName(String selectedProjectVersionName) {
        this.selectedProjectVersionName = selectedProjectVersionName;
    }


    public MetadataService getMetadataService() {
        return metadataService;
    }


    public void setMetadataService(MetadataService metadataService) {
        this.metadataService = metadataService;
    }


    public MetadataUtils getMetadataUtils() {
        return metadataUtils;
    }


    public void setMetadataUtils(MetadataUtils metadataUtils) {
        this.metadataUtils = metadataUtils;
    }


}
