package com.osoboo.jira.metadata.ui.internal;

import com.atlassian.jira.plugin.profile.OptionalUserProfilePanel;
import com.atlassian.jira.plugin.profile.ViewProfilePanel;
import com.atlassian.jira.plugin.profile.ViewProfilePanelModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.opensymphony.util.TextUtils;
import com.osoboo.jira.metadata.MetadataService;
import com.osoboo.jira.metadata.MetadataUtils;

import java.util.HashMap;
import java.util.Map;


/**
 * The User Metadata Tab.
 * 
 * @author aspall
 * 
 */
public class UserPanelMetadataPanel implements ViewProfilePanel, OptionalUserProfilePanel {

    private JiraAuthenticationContext authenticationContext;
    private ViewProfilePanelModuleDescriptor moduleDescriptor;
    private MetadataService metadataService;
    private PermissionManager permissionManager;


    public UserPanelMetadataPanel(JiraAuthenticationContext authenticationContext,
            PermissionManager permissionManager, MetadataService metadataService) {
        this.authenticationContext = authenticationContext;
        this.permissionManager = permissionManager;
        this.metadataService = metadataService;
    }


    @Override
    public boolean showPanel(ApplicationUser profileUser,ApplicationUser currentUser) {
        final ApplicationUser user = authenticationContext.getLoggedInUser();
        return profileUser.equals(currentUser)
                || MetadataHelper.userHasAdminPermission(permissionManager, authenticationContext);
    }


    @Override
    public String getHtml(ApplicationUser profileUser) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("metadataEditHiddenKey", "selectedUser");
        params.put("metadataEditHiddenValue", profileUser.getName());
        params.put("formActionSaveUrl", "SaveMetadataUserProfile.jspa");
        params.put("userHasAdminPermission", MetadataHelper.userHasAdminPermission(permissionManager, authenticationContext));
        params.put("textUtils", new TextUtils());
        params.put("metadataUtils", new MetadataUtils());
        params.put("metadatas", metadataService.getMetadata(profileUser));
        return moduleDescriptor.getHtml(VIEW_TEMPLATE, params);
    }


    @Override
    public void init(ViewProfilePanelModuleDescriptor moduleDescriptor) {
        this.moduleDescriptor = moduleDescriptor;
    }

}
