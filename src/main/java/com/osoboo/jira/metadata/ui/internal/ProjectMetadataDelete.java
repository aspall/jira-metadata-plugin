/*
 * Copyright (c) 2011, Andreas Spall
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Andreas Spall nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.osoboo.jira.metadata.ui.internal;

import java.text.MessageFormat;

import org.apache.log4j.Logger;

import webwork.action.ServletActionContext;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.osoboo.jira.metadata.MetadataService;

public class ProjectMetadataDelete extends JiraWebActionSupport {

	/** */
	private static final long serialVersionUID = 7861762358377860485L;

	private static final Logger log = Logger.getLogger(ProjectMetadataDelete.class);

	private int metadataKey;

	private String projectKey;

	private final MetadataService metadataService;

	private final PermissionManager permissionManager;

	private final JiraAuthenticationContext jiraAuthenticationContext;

	public ProjectMetadataDelete(PermissionManager permissionManager, JiraAuthenticationContext jiraAuthenticationContext,
			MetadataService metadataService) {
		this.permissionManager = permissionManager;
		this.jiraAuthenticationContext = jiraAuthenticationContext;
		this.metadataService = metadataService;
	}

	public int getMetadataKey() {
		return metadataKey;
	}

	public void setMetadataKey(int metadataKey) {
		this.metadataKey = metadataKey;
	}

	public String getProjectKey() {
		return projectKey;
	}

	public void setProjectKey(String projectKey) {
		this.projectKey = projectKey;
	}

	@Override
	protected String doExecute() throws Exception {
		Project project = getProjectManager().getProjectObjByKey(projectKey);
		if (MetadataHelper.userHasProjectAdminPermission(permissionManager, jiraAuthenticationContext, project)) {
			metadataService.delete(project, metadataKey);
		} else {
			log.warn(MessageFormat.format("The user {0} tried to delete the metadata with id {1} for the project {2} i'll ignore the call",
					getLoggedInUser().getName(), metadataKey, projectKey));
		}
		ServletActionContext.getResponse().sendRedirect(ServletActionContext.getRequest().getContextPath() + "/browse/" + projectKey);
		return NONE;
	}
}
