/*
 * Copyright (c) 2016, Andreas Spall
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Andreas Spall nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.osoboo.jira.metadata.ui.internal;

import com.atlassian.jira.bc.EntityNotFoundException;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.osoboo.jira.metadata.MetadataService;
import com.osoboo.jira.metadata.MetadataUtils;


public class ProjectComponentMetadataPanel extends JiraWebActionSupport {

    /** */
    private static final long serialVersionUID = 4424423287983643833L;
    private ProjectComponentManager projectComponentManager;
    private ProjectManager projectManager;

    private String selectedProjectComponentId;

    private String selectedProjectKey;
    private String selectedProjectComponentName;

    private ProjectComponent selectedProjectComponentObject;

    private MetadataService metadataService;

    private MetadataUtils metadataUtils;


    public ProjectComponentMetadataPanel(ProjectComponentManager projectComponentManager, ProjectManager projectManager,
            MetadataService metadataService) {
        this.projectComponentManager = projectComponentManager;
        this.projectManager = projectManager;
        this.metadataService = metadataService;
        this.metadataUtils = new MetadataUtils();
    }


    @Override
    public String doDefault() throws Exception {
        transformSelectedProjectComponent();
        return super.doDefault();
    }


    @Override
    protected String doExecute() throws Exception {
        transformSelectedProjectComponent();
        return super.doExecute();
    }


    private void transformSelectedProjectComponent() {
        if (selectedProjectComponentObject == null) {
            if (selectedProjectComponentId != null) {
                try {
                    selectedProjectComponentObject = projectComponentManager.find(Long.parseLong(selectedProjectComponentId));
                } catch (EntityNotFoundException e) {
                }
            }
            if (selectedProjectKey != null && selectedProjectComponentName != null) {
                Project projectByCurrentKey = projectManager.getProjectByCurrentKey(selectedProjectKey);
                if (projectByCurrentKey != null) {
                    selectedProjectComponentObject = projectComponentManager.findByComponentName(projectByCurrentKey.getId(),
                            selectedProjectComponentName);
                    if (selectedProjectComponentObject != null) {
                        selectedProjectComponentId = Long.toString(selectedProjectComponentObject.getId());
                    } else {
                        selectedProjectComponentId = null;
                    }
                }
            }
        } else {
            if (selectedProjectComponentId != null && selectedProjectComponentObject.getId() != Long.parseLong(
                    selectedProjectComponentId)) {
                try {
                    selectedProjectComponentObject = projectComponentManager.find(Long.parseLong(selectedProjectComponentId));
                } catch (EntityNotFoundException e) {
                }
            }
        }

    }


    public String getSelectedProjectComponentId() {
        return selectedProjectComponentId;
    }


    public void setSelectedProjectComponentId(String selectedProjectComponentId) {
        this.selectedProjectComponentId = selectedProjectComponentId;
    }


    public ProjectComponent getSelectedProjectComponentObject() {
        return selectedProjectComponentObject;
    }


    public void setSelectedProjectComponentObject(ProjectComponent selectedProjectComponentObject) {
        this.selectedProjectComponentObject = selectedProjectComponentObject;
    }


    public String getSelectedProjectKey() {
        return selectedProjectKey;
    }


    public void setSelectedProjectKey(String selectedProjectKey) {
        this.selectedProjectKey = selectedProjectKey;
    }


    public String getSelectedProjectComponentName() {
        return selectedProjectComponentName;
    }


    public void setSelectedProjectComponentName(String selectedProjectComponentName) {
        this.selectedProjectComponentName = selectedProjectComponentName;
    }


    public MetadataService getMetadataService() {
        return metadataService;
    }


    public void setMetadataService(MetadataService metadataService) {
        this.metadataService = metadataService;
    }


    public MetadataUtils getMetadataUtils() {
        return metadataUtils;
    }


    public void setMetadataUtils(MetadataUtils metadataUtils) {
        this.metadataUtils = metadataUtils;
    }


}
