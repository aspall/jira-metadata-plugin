/*
 * Copyright (c) 2011, Andreas Spall
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Andreas Spall nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.osoboo.jira.metadata.ui.field;

import java.util.List;
import java.util.Map;

import com.atlassian.core.util.XMLUtils;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.config.item.DefaultValueConfigItem;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.velocity.NumberTool;
import com.opensymphony.util.TextUtils;

@SuppressWarnings("unchecked")
public abstract class AbstractMetadataCFType extends CalculatedCFType {

	private GenericConfigManager genericConfigManager;

	protected AbstractMetadataCFType(GenericConfigManager genericConfigManager) {
		this.genericConfigManager = genericConfigManager;
	}

	/**
	 * @return contains the marker that the Field can be configured via default
	 *         value.
	 */
	@Override
	public List<FieldConfigItemType> getConfigurationItemTypes() {
		return CollectionBuilder.<FieldConfigItemType> newBuilder(JiraUtils.loadComponent(DefaultValueConfigItem.class)).asArrayList();
	}

	/**
	 * checks whether the given values can be transformed to Double (and
	 * compared) or calls super.compare.
	 */
	@Override
	public int compare(Object value1, Object value2, FieldConfig fieldConfig) {
		try {
			Double double1 = Double.valueOf(value1.toString());
			return double1.compareTo(Double.valueOf(value2.toString()));
		} catch (RuntimeException e) {
			return super.compare(value1, value2, fieldConfig);
		}
	}

	/**
	 * @return returns the super get velocity parameters and adds the
	 *         {@link NumberTool}, {@link TextUtils}, {@link XMLUtils}
	 */
	@Override
	public Map<String, Object> getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem) {
		Map<String, Object> values = super.getVelocityParameters(issue, field, fieldLayoutItem);
		if (!values.containsKey("numberTool")) { //$NON-NLS-1$
			values.put("numberTool", new NumberTool(getI18nBean().getLocale())); //$NON-NLS-1$
		}
		if (!values.containsKey("textutils")) { //$NON-NLS-1$
			values.put("textutils", new TextUtils()); //$NON-NLS-1$
		}
		if (!values.containsKey("xmlutils")) { //$NON-NLS-1$
			values.put("xmlutils", new XMLUtils()); //$NON-NLS-1$
		}
		return values;
	}

	/**
	 * @return the given object.
	 */
	@Override
	public Object getSingularObjectFromString(String arg0) throws FieldValidationException {
		return arg0;
	}

	/**
	 * @return calls toString on the given object or an empty string if the
	 *         object is null.
	 */
	@Override
	public String getStringFromSingularObject(Object singularObject) {
		if (singularObject == null) {
			return ""; //$NON-NLS-1$
		}
		return singularObject.toString();
	}

	/**
	 * Store the default value via generic config manager
	 */
	public void setDefaultValue(FieldConfig fieldConfig, Object value) {
		genericConfigManager.update(DEFAULT_VALUE_TYPE, fieldConfig.getId().toString(), value);
	}

	/**
	 * @return returns the default value via generic config manager
	 */
	public Object getDefaultValue(FieldConfig fieldConfig) {
		try {
			return genericConfigManager.retrieve(DEFAULT_VALUE_TYPE, fieldConfig.getId().toString());
		} catch (Exception e) {
			return null;
		}
	}

}
