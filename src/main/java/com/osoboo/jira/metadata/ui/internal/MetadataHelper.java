package com.osoboo.jira.metadata.ui.internal;


import com.atlassian.jira.user.ApplicationUser;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.browse.BrowseContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;


public class MetadataHelper {

    /** the project admin permission key. */
    private static final int PROJECT_ADMIN = 23;
    /** the jira admin permission key. */
    private static final int JIRA_ADMIN = 0;


    public static boolean userIsNotInRole(ProjectRoleManager projectRoleManager, PermissionManager permissionManager,
            JiraAuthenticationContext authenticationContext, BrowseContext ctx, String toCheckRoles) {
        if (!toCheckRoles.isEmpty()) {
            for (String role : toCheckRoles.split(",")) {
                ProjectRole externalProjectRole = projectRoleManager.getProjectRole(role.trim());
                if (externalProjectRole != null
                        && projectRoleManager.isUserInProjectRole(authenticationContext.getUser(), externalProjectRole, ctx.getProject())) {
                    return userHasProjectAdminPermission(permissionManager, authenticationContext, ctx);
                }
            }
        }
        return true;
    }

    public static boolean userHasAdminPermission(PermissionManager permissionManager, ApplicationUser applicationUser) {
        return applicationUser != null && permissionManager.hasPermission(JIRA_ADMIN, applicationUser);
    }

    public static boolean userHasAdminPermission(PermissionManager permissionManager, JiraAuthenticationContext authenticationContext) {
        return userHasAdminPermission(permissionManager, authenticationContext.getUser());
    }


    public static boolean userHasProjectAdminPermission(PermissionManager permissionManager,
            JiraAuthenticationContext authenticationContext, BrowseContext ctx) {
        return userHasProjectAdminPermission(permissionManager, authenticationContext, ctx.getProject());
    }


    public static boolean userHasProjectAdminPermission(PermissionManager permissionManager,
            JiraAuthenticationContext authenticationContext, Project project) {
        return permissionManager.hasPermission(PROJECT_ADMIN, project, authenticationContext.getUser());
    }

}
