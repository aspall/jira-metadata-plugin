/*
 * Copyright (c) 2016, Andreas Spall
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Andreas Spall nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.osoboo.jira.metadata.ui.internal;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.osoboo.jira.metadata.MetadataService;
import webwork.action.ServletActionContext;


public class ProjectComponentMetadataDelete extends JiraWebActionSupport {


    /**  */
    private static final long serialVersionUID = 1744678994064565119L;

    private int metadataKey;

    private String selectedProjectComponentId;

    private final MetadataService metadataService;

    private final PermissionManager permissionManager;

    private final JiraAuthenticationContext jiraAuthenticationContext;

    private ProjectComponentManager projectComponentManager;


    public ProjectComponentMetadataDelete(PermissionManager permissionManager, ProjectComponentManager projectComponentManager,
            JiraAuthenticationContext jiraAuthenticationContext, MetadataService metadataService) {
        this.permissionManager = permissionManager;
        this.projectComponentManager = projectComponentManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.metadataService = metadataService;
    }


    public int getMetadataKey() {
        return metadataKey;
    }


    public void setMetadataKey(int metadataKey) {
        this.metadataKey = metadataKey;
    }


    public String getSelectedProjectComponentId() {
        return selectedProjectComponentId;
    }


    public void setSelectedProjectComponentId(String selectedProjectComponentId) {
        this.selectedProjectComponentId = selectedProjectComponentId;
    }


    @Override
    protected String doExecute() throws Exception {
        ProjectComponent version = projectComponentManager.find(Long.parseLong(selectedProjectComponentId));
        if (MetadataHelper.userHasAdminPermission(permissionManager, jiraAuthenticationContext)) {
            metadataService.delete(version, metadataKey);
        }
        ServletActionContext.getResponse().sendRedirect(ServletActionContext.getRequest().getContextPath()
                + "/secure/admin/metadata/ProjectComponentAction.jspa?selectedProjectComponentId=" + selectedProjectComponentId);
        return NONE;
    }
}
