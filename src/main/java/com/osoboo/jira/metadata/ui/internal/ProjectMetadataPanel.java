/*
 * Copyright (c) 2011, Andreas Spall
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Andreas Spall nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.osoboo.jira.metadata.ui.internal;

import java.util.Map;

import com.atlassian.jira.plugin.projectpanel.impl.AbstractProjectTabPanel;
import com.atlassian.jira.project.browse.BrowseContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.util.JiraVelocityUtils;
import com.opensymphony.util.TextUtils;
import com.osoboo.jira.metadata.MetadataService;
import com.osoboo.jira.metadata.MetadataUtils;

/**
 * the project metadata panel adds the meta data service, and the result of the
 * check if user is project admin to the velocity parameter list.
 * 
 * @author aspall
 * 
 */
public class ProjectMetadataPanel extends AbstractProjectTabPanel {

	private MetadataService metadataService;
	/** used to check the current users permission to edit the metadata. */
	private final PermissionManager permissionManager;
	private final ProjectRoleManager projectRoleManager;

	public ProjectMetadataPanel(PermissionManager permissionManager, JiraAuthenticationContext jiraAuthenticationContext,
			MetadataService metadataService, ProjectRoleManager projectRoleManager) {
		super(jiraAuthenticationContext);
		this.permissionManager = permissionManager;
		this.metadataService = metadataService;
		this.projectRoleManager = projectRoleManager;

	}

	@Override
	protected Map<String, Object> createVelocityParams(BrowseContext ctx) {
		final Map<String, Object> params = JiraVelocityUtils.getDefaultVelocityParams(super.createVelocityParams(ctx),
				authenticationContext);
		params.put("i18n", authenticationContext.getI18nHelper());
		params.put("project", ctx.getProject());
		params.put("hasAdminPermission", MetadataHelper.userHasProjectAdminPermission(permissionManager, authenticationContext, ctx));
		params.put("metadataService", metadataService);
		params.put("textutils", new TextUtils());
		params.put("metadataUtils", new MetadataUtils());
		return params;
	}

	@Override
	public boolean showPanel(BrowseContext ctx) {
		String hideRoles = metadataService.getMetadataValue(ctx.getProject(), "jira.metadata.projectRole.hide.tab.project");
		return MetadataHelper.userIsNotInRole(projectRoleManager, permissionManager, authenticationContext, ctx, hideRoles);
	}


}
