/*
 * Copyright (c) 2013, Andreas Spall
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Andreas Spall nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.osoboo.jira.metadata.ui.internal;

import webwork.action.ServletActionContext;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.util.UserManager;
import com.osoboo.jira.metadata.MetadataService;

@SuppressWarnings("unchecked")
public class GroupMetadataSave extends AbstractMetadataSave {

	/** */
	private static final long serialVersionUID = 7861762358377860485L;

	private String selectedGroup;

	private UserManager myUserManager;

	public GroupMetadataSave(PermissionManager permissionManager,
			UserManager userManager,
			JiraAuthenticationContext jiraAuthenticationContext,
			MetadataService metadataService) {
		super(permissionManager, jiraAuthenticationContext, metadataService);
		this.myUserManager = userManager;
	}

	public String getSelectedGroup() {
		return selectedGroup;
	}

	public void setSelectedGroup(String selectedGroup) {
		this.selectedGroup = selectedGroup;
	}

	@Override
	protected String doExecute() throws Exception {

		Group group = myUserManager.getGroupObject(selectedGroup);
		if (MetadataHelper.userHasAdminPermission(permissionManager,
				jiraAuthenticationContext)) {
			metadataService.save(group, getMetadataKey(), getMetadataValue(),
					getMetadataGroup(), isMetadataHidden());
		}

		ServletActionContext
				.getResponse()
				.sendRedirect(
						ServletActionContext.getRequest().getContextPath()
								+ "/secure/admin/metadata/GroupAction.jspa?selectedGroup="
								+ selectedGroup);
		return NONE;
	}
}
