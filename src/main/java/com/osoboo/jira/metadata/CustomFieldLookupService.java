package com.osoboo.jira.metadata;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;

/**
 * The Jira Metadata Plugin customfield lookup service (get the data of other
 * customfields).
 * 
 * @author aspall
 * 
 */
public interface CustomFieldLookupService {

	/**
	 * @param issue
	 *            the issue
	 * @param customFieldId
	 *            the customField identifier.
	 * @return the customFields (identified by the customFieldId) value of the
	 *         given issue or null if something went wrong
	 * @see Issue#getCustomFieldValue(com.atlassian.jira.issue.fields.CustomField)
	 * @see CustomFieldManager#getCustomFieldObject(String)
	 */
	Object getCustomFieldValue(Issue issue, String customFieldId);

}
