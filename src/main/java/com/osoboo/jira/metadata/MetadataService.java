/*
 * Copyright (c) 2011, Andreas Spall
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Andreas Spall nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software 
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.osoboo.jira.metadata;

import java.util.Collection;

import com.atlassian.activeobjects.tx.Transactional;

/**
 * Will be used to load / store / delete the metadata.
 * 
 * @author aspall
 * 
 */
@Transactional
public interface MetadataService {

	/**
	 * saves the given value for the enrichedObject / key combination with
	 * hidden false
	 * 
	 * @param enrichedObject the enriched object (e.g. User, Project, ...)
	 * @param key
	 * @param value
	 */
	void save(Object enrichedObject, String key, String value);

	/**
	 * saves the given value for the enrichedObject / key combination.
	 * 
	 * @param enrichedObject the enriched object (e.g. User, Project, ...)
	 * @param key
	 * @param value
	 * @param hidden
	 *            true : the value should not be displayed to the user
	 */
	void save(Object enrichedObject, String key, String value, boolean hidden);

	/**
	 * saves the given value for the enrichedObject / key combination.
	 * 
	 * @param enrichedObject the enriched object (e.g. User, Project, ...)
	 * @param key
	 * @param value
	 * @param group
	 *            the shown metadata-group of this metadata (not part of the
	 *            key)
	 * @param hidden
	 *            true : the value should not be displayed to the user
	 */
	void save(Object enrichedObject, String key, String value, String group, boolean hidden);

	/**
	 * returns all non hidden metadatas for the given enriched Object
	 * 
	 * @param enrichedObject
	 * @return
	 */
	Collection<JiraMetadata> getMetadata(Object enrichedObject);

    /**
     * returns the non hidden metadatas for the given enriched Object
     * 
     * @param enrichedObject the enriched object (e.g. User, Project, ...)
     * @param key the userdefined key of the metadata
     * @return the JiraMetadata object or null
     */
    JiraMetadata getMetadata(Object enrichedObject, String key);

    /**
	 * returns all metadatas for the given enriched Object
	 * 
	 * @param enrichedObject
	 * @return
	 */
	Collection<JiraMetadata> getAllMetadata(Object enrichedObject);

	/**
	 * deletes the metadata specified by the enriched Object and the unique id
	 * of the metadata.
	 * 
	 * @param enrichedObject the enriched object (e.g. User, Project, ...)
	 * @param id
	 */
	void delete(Object enrichedObject, int id);

	/**
	 * returns the metadata value for the given enrichedObject / key
	 * combination.
	 * 
	 * @param enrichedObject the enriched object (e.g. User, Project, ...)
	 * @param key
	 * @return the metadata value or an empty string
	 */
	String getMetadataValue(Object enrichedObject, String key);
	
    Collection<JiraMetadata> getMetadata(String enrichedClassIdentifierAsString, String userKey, String userValue);

}
